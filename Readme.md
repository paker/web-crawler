## Web crawler

# A little about technologies:
* The project is implemented in **NodeJS**
* **ExpressJS** is used as web server
* **Redis** is used for job queues management

# Installation
There are 2 possible ways of installation: with Docker or locally

### Docker Installation

* Ensure you have a [NodeJS, npm](https://nodejs.org/en/) installed
* Ensure you have [docker-compose](https://docs.docker.com/compose/install) installed
* The following command will install node dependencies, build docker containers and start the web and Redis servers.


    `npm install && docker-compose up`

### Local Installation
 
* Ensure you have a [NodeJS, npm](https://nodejs.org/en/)installed
* Ensure you have [Redis](https://redis.io/download) installed (configure Redis URL in the **config/config.js**)
* Install app dependencies

    
    `npm install`
    
* If you want to start the web server

    
    `npm start`

# Running

### With web server
After servers start you can make HTTP request to add a job:

    http://127.0.0.1:3000/crawler?url=http://wikipedia.org&depth=2
        
### Command line without web server
Run following script to start a crawling job without web server

    ./bin/crawler-cli http://www.google.org 2
    

     
# Configuration
* See **config/config.js** for configuration options of Redis, Express and the crawler

# Project structure and implementation notes
* **bin/www** web server starting script (see instructions above)
* **bin/crawler-cli** cli mode starting script (see instructions above)


* **config/config.js** app configuration


* **data/** directory where downloaded files are saved (filesystem cache)
* **export/** directory where exported TSV files are saved (when job finishes)


* **routes/crawler.js** implements */crawler* API endpoint to add jobs to the crawler
* **routes/validationMiddleware.js** query validation for /crawler requests


## Crawler controller
* **controllers/crawlerController.js** 

The application main entry point.
Responsible for starting job and task controllers and defining a task worker.

#### Worker algorithm
Every worker gets one task and proceeds according to following algorithm:

* Download a web page.
    * If page was not modified since last download, use a cached version from a filesystem.
    * If downloaded, save response to the filesystem cache.
* Extract URL's from the web page.
* If depth of the current task less than required by the job, add new URL's to **PENDING** queue.
* Calculate same domain ratio for the page.


## Job Controller
* **controllers/jobController.js**

Job is a process of crawling the web starting from the given page till the given depth.
Every job has a jobId which is UUID generated on a job creation.
 
Job structure:

    {
        "jobId": "4b9f5489-c31a-41b4-97fe-c46fa1bf42ec",
        "url": "https://wikipedia.org",
        "depth": "2",
        "cli": "0",
        "exported": "true"
    }
* **url** the first page to crawl
* **depth** how deep to crawl, depth=1 to crawl initial page only
* **cli** is true when job added from a cli script
* **exported** indicates that job has finished and export TSV file was generated

Jobs list is saved in Redis under Set key named **LRJOBS**
Job details saved under Redis Hashes with keys in format `LRJOB:{JOBID}`

E.g.

    "LRJOB:4b9f5489-c31a-41b4-97fe-c46fa1bf42ec"

When a new job added, the first task created with initial URL.
Once all job tasks are completed, the result is exported to TSV file and process terminates in case it's called from command line.

## Cache Controller
* **controllers/cacheController.js**
 
File system cache implementation.

Every web page is saved to a file under a directory named corresponding to it's hostname.
File names are produced from URL's while stripping non-valid characters and truncating the name. (See also URI Controller below).

The cached data is used for repeated requests if page was not modified since the last download.
**Last-Modified** and **Etag** HTTP headers are used for this purpose.


## Download Controller
* **controllers/downloadController.js**

Uses **request** library to make GET HTTP requests and to download the web page data.
For caching purposes sets **If-Modified-Since** and **If-None-Match** HTTP headers to check if page was modified.
Saves only pages with **text/html** content type. 


## Export controller
* **controllers/exportController.js**

Saves TSV file with finished job data.

## Parser controller
* **controllers/parserController.js**

Parses HTML and extracts URL's from <a> tags.


## Redis Task/Job storage controller
* **controllers/redisController.js**

Manages jobs and tasks Redis data storage.
See Job Controller above and Task Controller below for more details.

## Task Controller
* **controllers/taskController.js**

Task execution is a process of downloading a single web page.

Task structure:
Every task is represented by following object structure:

    {
     "url": "http://www.google.org",
     "depth": "1",
     "jobId": "f4d6ff0c-6d3f-47fa-8f95-7eea7c1f007a"
    }

Additional data can be added during the task execution:

    {
     "url": "http://www.google.org",
     "depth": "1",
     "jobId": "f4d6ff0c-6d3f-47fa-8f95-7eea7c1f007a"
     "sameDomainRatio": "0.77"
     "lastModified": "Wed, 26 Jul 2017 10:30:00 GMT"
     "Etag": "AJHKJAHSAUHSIUA",
     "errorCode": "ECONNREFUSED",
     
    }

Tasks data is saved in Redis Hashes under keys in format `LRURL:{URL}`

E.g. `"LRURL:http://www.google.org/our-work/economic-opportunity/code-for-america/"`

#### Task flow

The task can reside in one of the following queues: 

    * PENDING
    * PROCESSING
    * FINISHED
    * FAILED 
    * INVALID


* First, task is added to **PENDING** queue by the Job Controller.
* The Task controller periodically checks for empty slots in the **PROCESSING** queue and moves tasks from **PENDING** to **PROCESSING**.
* When worker completes it's job successfully, the task is moved to the **FINISHED** queue.
* If an error occurs, the task moved to **FAILED** or **INVALID** queue. 
   * The difference is that **FAILED** tasks are retried, while **INVALID** are not.
   * There are error types that will move the task directly to **INVALID** queue without retries.
* When all workers complete execution, the recovery process starts. It moves **FAILED** tasks to the **PENDING** queue for re-execution.
* When application is restarted all the old **PROCESSING** jobs are moved to the **PENDING** queue for re-execution.

## URI Controller
* **controllers/URIController.js**

URI controller is responsible for URI manipulation.
E.g. same domain ratio calculation, building URLs from the parsed links (converting relative links to absolute, addition of protocol).

The way URL's are converted to file names is not ideal. 
It can produce non-unique file names from different URLs and in production solution some unique hash should be used for the file naming.