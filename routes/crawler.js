const express = require("express");

const queryValidation = require("./validationMiddleware");
const crawlerController = require("../controllers/crawlerController");
const jobController = require("../controllers/jobController");
const URIController = require("../controllers/URIController");

const router = express.Router();

router.get("/", queryValidation.validateCrawlerQuery);

router.get("/", async (req, res, next) => {
    const url = req.query.url;
    const depth = parseInt(req.query.depth);

    if(!URIController.validateURL(url)){
        return res.json({error:"Invalid URL (e.g. http://example.com)", url: url});
    }

    const job = jobController.addJob(url, depth);
    res.json(job);
});

crawlerController.run();

module.exports = router;
