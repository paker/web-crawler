const requiredCrawlerParams = ["url", "depth"];

const validationMiddleware = {
    validateCrawlerQuery (req, res, next) {
        if (!validationMiddleware.validateQueryParams(req.query, requiredCrawlerParams)) {
            return res.status(400).send("Invalid query. Required parameters: " + requiredCrawlerParams.join(", "));
        }

        next();
    },
    
    validateQueryParams (query, requiredParams) {
        if (!query) {
            return false;
        }

        let valid = true;
        requiredParams.forEach((param) => {
            if (!(param in query)) {
                valid = false;
            }
        });

        return valid;
    }
};

module.exports = validationMiddleware;
