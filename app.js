const express = require("express");
const logger = require("winston");

const config = require("./config/config");
const crawler = require("./routes/crawler");

const app = express();

app.use("/crawler", crawler);

// Not Found error handling
app.use((req, res, next) => {
    let err = new Error("Not Found");
    err.status = 404;
    next(err);
});

// Server errors handling
app.use((err, req, res, next) => {
    logger.log("error", "Server error", {error: err});
    if (!("status" in err)){
        err.status = 500
    }
    res.status(err.status).send("Server error. We have a problem ;-( ");
});

// Web server starting
app.listen(config.EXPRESS_PORT, () => {
    logger.log("info", `Express web server is listening on port ${config.EXPRESS_PORT}`);
});

process.on("unhandledRejection", (err) => {
    logger.log("error", err);
});

process.on("unhandledException", (err) => {
    logger.log("error", err);
    // worker.exit(1);
});

module.exports = app;
