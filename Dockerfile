FROM node:8.6
WORKDIR /usr/src/app
COPY package.json .
COPY package.json package-lock.json ./
RUN npm install
COPY . .
EXPOSE 3000
CMD [ "npm", "start" ]
