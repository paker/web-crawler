const logger = require("winston");
const readFile = require("fs-readfile-promise");
const fs = require("fs");
const path = require("path");

const config = require("../config/config");
const URI = require("./URIController");

const cacheController = module.exports = {
    isCached(response) {
        return "cached" in response && response["cached"] === true;
    },
    async getCachedResponse(task, response) {
        if (cacheController.isCached(response)) {
            response = cacheController.copyCacheHeaders(task, response);
            const cachedBody = await cacheController.getCachedData(task.url);
            response.body = cachedBody.toString();
        }

        return response;
    },
    copyCacheHeaders(source, destination) {
        if ('etag' in source) {
            destination.etag = source.etag;
        }
        if ('lastModified' in source) {
            destination.lastModified = source.lastModified;
        }

        return destination;
    },
    async getCachedData(url) {
        const fileName = URI.URLToFilename(url);
        const filePath = path.join(config.DATADIR_PATH, URI.URLToPath(url), fileName);
        try {
            return await readFile(filePath);
        } catch (error) {
            logger.log("error", "Error reading cached response from filesystem", url, filePath, error);
        }
    },
    saveCachedData(url, data) {
        const dirPath = path.join(config.DATADIR_PATH, URI.URLToPath(url));
        if (!fs.existsSync(dirPath)) {
            fs.mkdirSync(dirPath);
        }

        const fileName = URI.URLToFilename(url);
        const filePath = path.join(dirPath, fileName);
        fs.writeFile(filePath, data, (error) => {
            if (error) {
                logger.log("error", `Error saving file ${filePath}`, error);
            }
        });
    }
};
