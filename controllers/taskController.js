const async = require("async");
const logger = require("winston");

const config = require("../config/config");
const redis = require("./redisController");
const URI = require("./URIController");

let taskQueue = null;

const taskController = module.exports = {
    async printStatus() {
        logger.log("info", "PENDING", await redis.getSetCount(config.PENDING_QUEUE),
            "PROCESSING", await redis.getSetCount(config.PROCESSING_QUEUE),
            "FINISHED", await redis.getSetCount(config.FINISHED_QUEUE),
            "FAILED", await redis.getSetCount(config.FAILED_QUEUE),
            "INVALID", await redis.getSetCount(config.INVALID_QUEUE),
            "TasksInQueue", taskQueue._tasks.length
        );
    },
    init(worker) {
        taskQueue = async.queue((task, onFinishCallback) => {
            // Wrapper to use callback in async.queue async worker function
            worker(task, onFinishCallback);
        }, config.TASKS_PARALLEL_LIMIT);

        taskController.recovery(config.PROCESSING_QUEUE);

        setInterval(() => {
            const emptySlots = config.TASKS_PARALLEL_LIMIT - taskQueue._tasks.length;
            if (emptySlots > 0) {
                taskController.addTasksToProcessingQueue(emptySlots);
            }
        }, config.TASK_UPDATE_MSEC);

        setInterval(() => {
            taskController.recovery(config.FAILED_QUEUE);
        }, 20000);
    },
    async recovery(queue) {
        if (taskQueue._tasks.length > 0) {
            return;
        }
        taskController.printStatus();

        const failedTasks = await redis.getSetItems(config.REDIS_TYPE_TASK, queue);
        if (failedTasks.length > 0) {
            redis.addTasksToPending(failedTasks, true);
        }
    },
    async addTasksToProcessingQueue(taskLimit = config.TASKS_PARALLEL_LIMIT) {
        let tasks = await redis.getSetItems(config.REDIS_TYPE_TASK, config.PENDING_QUEUE, taskLimit);
        tasks = tasks.map((task) => {
            task.depth = parseInt(task.depth);
            return task;
        });

        redis.moveTasks(config.PENDING_QUEUE, config.PROCESSING_QUEUE, tasks);
        taskQueue.push(tasks, taskController.taskFinishedCallback);

        taskController.printStatus();
    },
    taskFinishedCallback(error, task) {
        if (error) {
            if ("code" in error) {
                task.errorCode = error.code;
            } else if ("statusCode" in error) {
                task.errorCode = error.statusCode;
            } else if ("error" in error && "code" in error.error) {
                task.errorCode = error.error.code;
            }

            logger.log("error", "Error in task", task.url, task.errorCode);
            logger.log("debug", "Full error", error);

            if (config.RETRIABLE_ERRORS_CODES.indexOf(task.errorCode) !== -1) {
                if (!('retry' in task)) {
                    task.retry = 1;
                }

                if (task.retry <= config.RETRIES_NUMBER) {
                    task.retry++;
                    redis.moveTasks(config.PROCESSING_QUEUE, config.FAILED_QUEUE, [task]);
                } else {
                    redis.moveTasks(config.PROCESSING_QUEUE, config.INVALID_QUEUE, [task]);
                }
            } else {
                redis.moveTasks(config.PROCESSING_QUEUE, config.INVALID_QUEUE, [task]);
            }
        } else {
            redis.moveTasks(config.PROCESSING_QUEUE, config.FINISHED_QUEUE, [task]);
        }
    },
    buildNewTasks(parentTask, newURLs) {
        const newTasks = newURLs.map((newURL) => {
            newURL = URI.normalizeProtocol(parentTask.url, newURL);
            return {url: URI.normalizeURLEndSlash(newURL), depth: parentTask.depth + 1, jobId: parentTask.jobId};
        });

        return newTasks;
    },
    newTask(url, depth, jobId) {
        return {url: url, depth: depth, jobId: jobId};
    }
};
