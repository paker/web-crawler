const csv = require("csv");
const fs = require("fs");
const path = require("path");
const logger = require("winston");

const config = require("../config/config");
const redis = require("./redisController");

const exportController = module.exports = {
    async export(jobId, finishedCallback) {
        let finishedTasks = await exportController.getFinishedTasksData();

        finishedTasks = finishedTasks.filter((job) => {
            return job.jobId === jobId;
        });

        if (finishedTasks.length === 0) {
            return finishedCallback();;
        }

        const columns = {
            url: "url",
            depth: "depth",
            sameDomainRatio: "ratio"
        };

        const options = {delimiter: "\t", header: true, columns: columns};
        csv.stringify(finishedTasks, options, (error, output) => {
            if (error) {
                logger.log("error", "Error creating export file", error)
            }
            const dirPath = config.EXPORTDIR_PATH;
            if (!fs.existsSync(dirPath)) {
                fs.mkdirSync(dirPath);
            }
            const fileName = `${jobId}.tsv`;
            const exportPath = path.join(dirPath, fileName);

            fs.writeFile(exportPath, output, (error) => {
                if (error) {
                    logger.log("error", `Error saving file ${exportPath}`, error);
                }
                logger.log("info", `Saved file ${exportPath}`);
                if (typeof finishedCallback === "function") {
                    finishedCallback();
                }
            });
        });
    },
    getFinishedTasksData() {
        return redis.getSetItems(config.REDIS_TYPE_TASK, config.FINISHED_QUEUE);
    },
};
