const logger = require("winston");
const redis = require("redis");
const bluebird = require("bluebird");

const config = require("../config/config");

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
const redisClient = redis.createClient(config.REDIS_URL);

redisClient.on("connect", () => {
    console.log("Redis: Redis connected");
});

redisClient.on("error", (err) => {
    console.log("Redis: Error " + err);
});

redisClient.on("end", () => {
    console.log("Redis: connection closed");
});

const redisController = module.exports = {
    getSetCount(key) {
        return redisClient.scardAsync(key);
    },
    moveTasks(set_from, set_to, tasks) {
        tasks.forEach((task) => {
            const key = task.url;
            redisClient.smoveAsync(set_from, set_to, key);
            redisController.setHashData(config.REDIS_TYPE_TASK, key, task);
        })
    },
    addTasksToPending(tasks, recovery = false) {
        tasks.forEach(async (task) => {
            const key = task.url;

            logger.log("info", "Adding task: ", task.url);
            if (await redisClient.sismemberAsync(config.PENDING_QUEUE, key)) {
                // Already in PENDING
            } else if (!recovery && await redisClient.sismemberAsync(config.PROCESSING_QUEUE, key)) {
                // Already in PROCESSING
            } else if (await redisClient.sismemberAsync(config.FINISHED_QUEUE, key)) {
                const finishedTasks = await redisController.getHashData(config.REDIS_TYPE_TASK, [key]);

                if (finishedTasks[0].jobId === task.jobId) {
                    // Already in FINISHED (SAME job)
                } else {
                    // Already in FINISHED (ANOTHER job), move to PENDING
                    redisClient.smoveAsync(config.FINISHED_QUEUE, config.PENDING_QUEUE, key)
                    redisController.setHashData(config.REDIS_TYPE_TASK, key, task);
                }
            } else if (await redisClient.sismemberAsync(config.FAILED_QUEUE, key)) {
                // Already in FAILED, move to PENDING
                redisClient.smoveAsync(config.FAILED_QUEUE, config.PENDING_QUEUE, key);
            } else {
                // Not in other QUEUES, move to PENDING
                redisController.setHashData(config.REDIS_TYPE_TASK, key, task);
                redisClient.saddAsync(config.PENDING_QUEUE, key);
            }
        })
    },
    addJob(job) {
        const key = job.jobId;
        redisController.setHashData(config.REDIS_TYPE_JOB, key, job);
        redisClient.saddAsync(config.REDIS_SET_JOBS, key);
    },
    async getSetItems(hashType, setKey, count = null) {
        let hashKeys = [];
        if (!count) {
            hashKeys = await redisClient.smembersAsync(setKey);
        }
        else {
            hashKeys = await redisClient.srandmemberAsync(setKey, count);
        }
        return redisController.getHashData(hashType, hashKeys);
    },
    setHashData(type, key, data) {
        redisClient.hmsetAsync(`${type}:${key}`, data)
    },
    getHashData(type, keys) {
        const multi = redisClient.multi();
        keys.forEach((key) => {
            multi.hgetall(`${type}:${key}`);
        });

        return multi.execAsync();
    }
};
