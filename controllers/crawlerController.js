const logger = require("winston");

const parseController = require("./parserController");
const redis = require("./redisController");
const URI = require("./URIController");
const cacheController = require("./cacheController");
const taskController = require("./taskController");
const downloadController = require("./downloadController");
const jobController = require("./jobController");

const crawlerController = module.exports = {
    run() {
        jobController.init();
        taskController.init(crawlerController.worker);
    },
    async worker(task, onFinishCallback) {
        try {
            let response = await downloadController.fetch(task);
            if (!response) {
                logger.log("error", "Error processing task: no response data", task);
                return onFinishCallback(new Error("Error: no response data"), task);
            }
            response = await cacheController.getCachedResponse(task, response);
            if ("body" in response) {
                if (!cacheController.isCached(response)) {
                    cacheController.saveCachedData(task.url, response.body);
                }

                const newURLs = parseController.extractURLs(response.body);
                const newURLsUnique = Array.from(new Set(newURLs));

                const newTasks = taskController.buildNewTasks(task, newURLsUnique);
                const job = await jobController.getJobById(task.jobId);
                if (task.depth < job.depth) {
                    redis.addTasksToPending(newTasks);
                }
                task.sameDomainRatio = URI.calculateSameDomainRatio(task.url, newTasks);
                task = cacheController.copyCacheHeaders(response, task);
            }
            return onFinishCallback(null, task);
        } catch (error) {
            return onFinishCallback(error, task);
        }
    }
};
