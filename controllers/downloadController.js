const logger = require("winston");
const request = require("request-promise");

const config = require("../config/config");

downloadController = module.exports = {
    async fetch(task) {
        try {
            let requestOptions = {
                resolveWithFullResponse: true,
                uri: task.url,
                simple: true,
                headers: [],
                timeout: config.REQUEST_TIMEOUT
            };

            if ("etag" in task) {
                requestOptions.headers["If-None-Match"] = task.etag;
            }
            if ("lastModified" in task) {
                requestOptions.headers["If-Modified-Since"] = task.lastModified;
            }

            const response = await request(requestOptions);
            if (response.headers["content-type"].startsWith("text/html") && response.statusCode === 200) {
                let responseData = {body: response.body};
                if ("etag" in response.headers) {
                    responseData["etag"] = response.headers["etag"];
                }
                if ("last-modified" in response.headers) {
                    responseData["lastModified"] = response.headers["last-modified"];
                }

                return responseData;
            }

            throw {
                message: "Invalid content-type, download not supported",
                code: "INVALIDCONTENTTYPE",
                contentType: response.headers["content-type"]
            }
        } catch (error) {
            if ("statusCode" in error && error["statusCode"] === 304) {
                logger.log("info", "304 Not modified", task.url);
                return {cached: true};
            }

            throw error;
        }
    }
};
