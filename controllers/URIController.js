const URLUtil = require("url");
const sanitizeFilename = require("sanitize-filename");

const URIController = module.exports = {
    validateURL(URL) {
        try {
            new URLUtil.URL(URL);
            return true;
        } catch (error) {
            return false;
        }
    },
    normalizeProtocol(parentURL, newURL) {
        // TODO: use URLUtil for URL construction

        const newURLProtocol = URLUtil.parse(newURL).protocol;
        if (newURLProtocol === null) {
            const parentURLParsed = URLUtil.parse(parentURL);

            const protocol = parentURLParsed.protocol;
            if (newURL.startsWith('//')) {
                newURL = `${protocol}${newURL}`;
            } else {
                const host = parentURLParsed.host;
                if (newURL.startsWith('/')) {
                    newURL = `${protocol}//${host}${newURL}`;
                } else {
                    newURL = `${protocol}//${host}/${newURL}`;
                }
            }
        }
        return newURL;
    },
    normalizeURLEndSlash(URL) {
        if (URL.endsWith('/')) {
            URL = URL.slice(0, -1);
        }

        return URL;
    },
    URLToFilename(URL) {
        return sanitizeFilename(URL);
    },
    URLToPath(URL) {
        // TODO: Use better unique filename
        return sanitizeFilename(URIController.getHostname(URL));
    },
    getHostname(URL) {
        return URLUtil.parse(URL).hostname;
    },
    calculateSameDomainRatio(URL, newURLsData) {
        let sameHostnameCount = 0;
        const hostname = URIController.getHostname(URL);
        newURLsData.forEach((newURL) => {
            if (hostname === URIController.getHostname(newURL.url)) {
                sameHostnameCount++;
            }
        });

        return (sameHostnameCount / newURLsData.length).toFixed(2);
    }
};
