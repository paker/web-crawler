const uuid = require("uuid/v4");
const logger = require("winston");

const config = require("../config/config");
const exportController = require("./exportController");
const redis = require("./redisController");
const URI = require("./URIController");
const taskController = require("./taskController");

const jobController = module.exports = {
    init() {
        // Register onAllJobsFinished event
        setInterval(async () => {
            const failed = await redis.getSetCount(config.FAILED_QUEUE);
            const pending = await redis.getSetCount(config.PENDING_QUEUE);
            const processing = await redis.getSetCount(config.PROCESSING_QUEUE);

            if (failed + pending + processing === 0) {
                jobController.onAllJobsFinished()
            }
        }, config.JOBS_FINISHED_CHECK_MSEC);
    },
    addJob(url, depth, cli = 0) {
        const newJob = jobController.newJob(url, depth, cli ? 1 : 0);
        redis.addJob(newJob);

        const newTask = taskController.newTask(newJob.url, 1, newJob.jobId);
        redis.addTasksToPending([newTask]);

        return newJob;
    },
    newJob(url, depth, cli) {
        url = URI.normalizeURLEndSlash(url);
        return {jobId: uuid(), url: url, depth: depth, cli: cli};
    },
    async getJobById(jobId) {
        const jobs = await redis.getHashData(config.REDIS_TYPE_JOB, [jobId]);
        let job = jobs[0];
        job.depth = parseInt(job.depth);
        return job;
    },
    async onAllJobsFinished() {
        const jobs = await redis.getSetItems(config.REDIS_TYPE_JOB, config.REDIS_SET_JOBS);
        jobs.forEach((job) => {
            if (!("exported" in job && parseInt(job["exported"]) === 1)) {
                logger.log("info", `Job finished:`, job);
                job.exported = 1;

                let jobFinishedCallback = () => {
                    if ("cli" in job && parseInt(job["cli"]) === 1) {
                        logger.log("info", "Exiting cli job...");
                        process.exit(0);
                    }
                };
                exportController.export(job.jobId, jobFinishedCallback);
                redis.setHashData(config.REDIS_TYPE_JOB, job.jobId, job);
            }
        });
    }
};
