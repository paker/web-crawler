const cheerio = require("cheerio");

const parserController = module.exports = {
    extractURLs(body) {
        const $ = cheerio.load(body);
        const links = $("a").map((i, link) => {
            return $(link).attr("href");
        }).get();
        return links;
    }
};
